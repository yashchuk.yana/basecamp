#pragma once
#include "BsonElement.h"

namespace bsonlib {
	/** BSON Object class for storing raw binary data and serealizing/deserialing 
		For all format specifications see http://bsonspec.org/spec.html	
	*/
	class BsonObj {
		int totalSize = 4; // for storing size of an object
		std::vector<BsonElement> bson;

		/** Extracts utf-8 string from input stream
			must start with its size at 4 first positions
			Stream must be opened and closed by caller
		*/
		std::string extractStrFromStream(std::istream &in);

		/** Updates size of an object by adding given value to it */
		void increaseSize(int valToIncrease);

		/** Template for adding simple key-value pairs to the end of the object - before EOO */
		template <typename T>
		void appendValue(std::string key, T value) {
			BsonElement elem(key, value);
			this->bson.insert(this->bson.end() - 1, elem);
			this->increaseSize(elem.getSize());
		}

	public:
		/** Constructs empty object, appends EOO */
		BsonObj();

		/** Constructs object from a given stream
			Stream must be opened and closed by caller
		*/
		BsonObj(std::istream &in);
		
		/** Returns human-readable representation of an object */
		std::string toString();

		/** Returns object as raw bytes in BSON format */
		std::vector<unsigned char> getBson();

		/** Exports object to a given stream as raw BSON 
			Stream must be opened and closed by caller
		*/
		void exportToStream(std::ostream &out);

		/** Appends new element to the end of the object */
		void appendBsonElement(BsonElement &elem);

		/** Methods for appending values of different types to the object */
		void appendEOO();
		void appendDouble(std::string key, double value);
		void appendUTF8(std::string key, std::string value);
		void appendDocument(std::string key, BsonObj document);
		void appendArray(std::string key, std::vector<BsonElement> values);
		void appendBinary(std::string key, int size, bsonsubtype subtype, unsigned char * bindata);
		void appendOIDFromStr(std::string key, std::string oid);
		void appendOIDFromArray(std::string key, unsigned char * oid);
		void appendBool(std::string key, bool value);
		void appendUTCDateTime(std::string key, int64_t seconds);
		void appendNull(std::string key);
		void appendRegex(std::string key, std::string regex, std::string regexOptions = "");
		void appendJSCode(std::string key, std::string code);
		void appendJSCodeScope(std::string key, std::string code, BsonObj *document = nullptr);
		void appendInt32(std::string key, int32_t value);
		void appendTimestamp(std::string key, uint64_t timestamp);
		void appendInt64(std::string key, int64_t value);
		void appendMinKey(std::string key);
		void appendMaxKey(std::string key);
		/** Appends EOO for ending embedded object */
		void appendEndArray();

		/** Checks if object contains field with a given name */
		bool hasField(std::string key);

		/** Returns element with a given fieldName, 
			if none was found returns empty BsonElement - isEmpty() is true
		*/
		BsonElement getField(std::string key);

		/** Returns embedded object with a given key,
			if none was found returns empty BsonObj - isEmpty() is true
		*/
		BsonObj getObjectField(std::string key);

		/** Returns element with a given key, supports search in nested objects
			to find 1 in {"obj" : {"x": 1 }} use key "obj.x"
			if none was found returns empty BsonElement - isEmpty() is true
		*/
		BsonElement getFieldDotted(std::string key);

		/** Returns element with a given key 
			if none was found returns empty BsonElement - isEmpty() is true
		*/
		BsonElement operator[](std::string key);

		/** Return total size of the object */
		int getSize();

		/** Returns true if first element is EOO */
		bool isEmpty();
	};
}