#include "BsonElement.h"

namespace bsonlib {
	BsonElement::BsonElement() {
		this->type = bsontype::EOO;
		this->size = 1;
	}

	BsonElement::BsonElement(std::string fname, bsontype type) { 
		this->type = type;
		this->fieldName = fname;
		this->fieldName += '\0';
		this->size += this->fieldName.size();
	}

	BsonElement::BsonElement(std::string fname, int32_t value, bool isArray, bool isDoc, bool isScope) {
		if (isArray)
			this->init(fname, value, bsontype::ARRAY);
		else if (isDoc)
			this->init(fname, value, bsontype::DOCUMENT);
		else if (isScope)
			this->init(fname, value, bsontype::JSCODESCOPE);
		else
			this->init(fname, value, bsontype::INT32BIT);
	}

	BsonElement::BsonElement(std::string fname, int64_t value, bool UTCDateTime) {
		UTCDateTime ? this->init(fname, value, bsontype::UTCDATETIME) : this->init(fname, value, bsontype::INT64BIT);
	}

	BsonElement::BsonElement(std::string fname, uint64_t value) {
		this->init(fname, value, bsontype::TIMESTAMP);
	}

	BsonElement::BsonElement(std::string fname, double value) {
		this->init(fname, value, bsontype::DOUBLE64BIT);
	}

	BsonElement::BsonElement(std::string fname, std::string value, bool isOID, bool isCode) {
		this->fieldName = fname;
		this->fieldName += '\0';
		if (isOID) {
			this->type = bsontype::OBJECTID;
			const char *p = value.c_str(); 
			for (size_t i = 0; i < value.size(); i++) { // converting 24-chars hex string to 12-bit object id
				std::string temp;
				temp += p[i];
				temp += p[++i];
				unsigned char byte = (unsigned char)std::stoi(temp, 0, 16);
				this->value.push_back(byte);
			}
		}
		else {
			if (isCode)
				this->type = bsontype::JSCODE;
			else
				this->type = bsontype::UTF8STRING;
			int size = value.size() + 1;
			this->value.push_back(size);
			this->value.push_back(size >> 8);
			this->value.push_back(size >> 16);
			this->value.push_back(size >> 24);
			this->value.insert(this->value.end(), value.begin(), value.end());
			this->value.push_back(0);
		}
		this->size += this->value.size() + this->fieldName.size();
	}

	BsonElement::BsonElement(std::string fname, std::string regex, std::string regexOptions) {
		this->type = bsontype::REGEX;
		this->fieldName = fname;
		this->fieldName += '\0';
		this->value.insert(this->value.end(), regex.begin(), regex.end());
		this->value.push_back(0);
		this->value.insert(this->value.end(), regexOptions.begin(), regexOptions.end());
		this->value.push_back(0);
		this->size += this->value.size() + this->fieldName.size();
	}

	BsonElement::BsonElement(std::string fname, unsigned char * oid) {
		this->type = bsontype::OBJECTID;
		this->fieldName = fname;
		this->fieldName += '\0';
		this->value.insert(this->value.end(), oid, oid + OID_SIZE);
		this->size += this->value.size() + this->fieldName.size();
	}

	BsonElement::BsonElement(std::string fname, bool value) {
		this->type = bsontype::BOOLFALSETRUE;
		this->fieldName = fname;
		this->fieldName += '\0';
		this->value.push_back(value);
		this->size += this->value.size() + this->fieldName.size();
	}

	BsonElement::BsonElement(std::string fname, int size, bsonsubtype subtype, unsigned char * bindata) {
		this->type = bsontype::BINARYDATA;
		this->fieldName = fname;
		this->fieldName += '\0';
		this->value.push_back(size);
		this->value.push_back(size >> 8);
		this->value.push_back(size >> 16);
		this->value.push_back(size >> 24);
		this->value.push_back(subtype);
		this->value.insert(this->value.end(), bindata, bindata + size);
		this->size += this->value.size() + this->fieldName.size();
	}

	std::string BsonElement::getFieldName() {
		return this->fieldName.substr(0, fieldName.size() - 1);
	}

	void BsonElement::setFieldName(std::string fieldName) {
		this->size -= this->fieldName.size(); // substract old value
		this->fieldName = fieldName;
		if (this->type != bsontype::EOO && this->type != bsontype::JSCODESCOPE) {
			this->fieldName += '\0';
			this->size += fieldName.size() + 1; // add new
		}
	}

	bsontype BsonElement::getType() {
		return this->type;
	}

	void BsonElement::setType(bsontype type) {
		this->type = type;
	}

	int BsonElement::getSize() {
		return this->size;
	}

	std::vector<unsigned char> BsonElement::getValue() {
		return this->value;
	}

	void BsonElement::setValue(unsigned char * data, int size) {
		this->size -= this->value.size(); // substract old value
		this->size += size; // add new
		this->value.insert(this->value.end(), data, data + size);
	}

	void BsonElement::setBoolValue(bool value) {
		this->size -= this->value.size(); // substract old value
		this->size += 1; // add new
		this->value.insert(this->value.begin(), value);
	}


	std::string BsonElement::getUTF() {
		if (this->type == bsontype::UTF8STRING || this->type == bsontype::REGEX ||
			this->type == bsontype::JSCODE || this->type == bsontype::OBJECTID) {
			std::string str(this->value.begin() + 4, this->value.end());
			return str;
		}
		else
			return "\0";
	}

	int BsonElement::getInt() {
		int value = 0;
		if (this->type == bsontype::INT32BIT || this->type == bsontype::ARRAY ||
			this->type == bsontype::DOCUMENT || this->type == bsontype::JSCODESCOPE) {
			for (int i = 0; i < sizeof(int); ++i)
				value += ((int)this->value[i]) << (8 * i);
		}
		return value;
	}

	int64_t BsonElement::getInt64() {
		int64_t value = 0;
		if (this->type == bsontype::INT64BIT || this->type == bsontype::UTCDATETIME) 
			memcpy(&value, &this->value.at(0), sizeof(int64_t));
		return value;
	}

	double BsonElement::getDouble() {
		double value = 0;
		if (this->type == bsontype::DOUBLE64BIT)
			memcpy(&value, &this->value.at(0), sizeof(double));
		return value;
	}
	

	std::string BsonElement::toString() {
		std::string elemStr;
		static bool arrayOpened;
		switch (this->type) {
		case bsontype::DOUBLE64BIT: {
			elemStr += (std::to_string(bsontype::DOUBLE64BIT));
			elemStr += " " + this->fieldName + " : ";
			elemStr += std::to_string(this->getDouble());
			break;
		}
		case bsontype::UTF8STRING: {
			elemStr += (std::to_string(bsontype::UTF8STRING));
			elemStr += " " + this->fieldName + " : ";
			elemStr += this->getUTF();
			break;
		}
		case bsontype::DOCUMENT: {
			elemStr += (std::to_string(bsontype::DOCUMENT));
			elemStr += " " + this->fieldName + " : {";
			break;
		}
		case bsontype::ARRAY: {
			elemStr += (std::to_string(bsontype::ARRAY));
			elemStr += " " + this->fieldName + " : [";
			arrayOpened = true;
			break;
		}
		case bsontype::BINARYDATA: {
			elemStr += (std::to_string(bsontype::BINARYDATA));
			elemStr += " " + this->fieldName + " : ";
			int size = this->getInt();
			elemStr += std::to_string(size) + " ";
			elemStr += (std::to_string(this->value[4])) + ": ";
			elemStr.insert(elemStr.end(), this->value.begin() + 5, this->value.end());
			break;
		}
		case bsontype::OBJECTID: {
			elemStr += (std::to_string(bsontype::OBJECTID));
			elemStr += " " + this->fieldName + " : ";
			std::string str;
			str.resize(this->value.size() * 2, ' ');
			static const std::string hex_char = "0123456789abcdef";

			for (size_t i = 0; i < this->value.size(); ++i) // converting raw binary to hex
			{
				str[i << 1] = hex_char[(this->value[i] >> 4) & 0x0f];
				str[(i << 1) + 1] = hex_char[this->value[i] & 0x0f];
			}
			elemStr += str;
			break;
		}
		case bsontype::BOOLFALSETRUE: {
			elemStr += (std::to_string(bsontype::BOOLFALSETRUE));
			elemStr += " " + this->fieldName + " : ";
			this->value[0] ? elemStr += "true" : elemStr += "false";
			break;
		}
		case bsontype::UTCDATETIME: {
			elemStr += (std::to_string(bsontype::UTCDATETIME));
			elemStr += " " + this->fieldName + " : ";
			elemStr += std::to_string(this->getInt64());
			break;
		}
		case bsontype::NULLVALUE: {
			elemStr += (std::to_string(bsontype::NULLVALUE));
			elemStr += " " + this->fieldName + " : ";
			elemStr += "null";
			break;
		}
		case bsontype::REGEX: {
			elemStr += (std::to_string(bsontype::REGEX));
			elemStr += " " + this->fieldName + " : ";
			elemStr.insert(elemStr.end(), this->value.begin(), this->value.end());
			break;
		}
		case bsontype::JSCODE: {
			elemStr += (std::to_string(bsontype::JSCODE));
			elemStr += " " + this->fieldName + " : ";
			elemStr += this->getUTF();
			break;
		}
		case bsontype::JSCODESCOPE: {
			elemStr += (std::to_string(bsontype::JSCODESCOPE));
			elemStr += " scope: ";
			elemStr += std::to_string(this->getInt()) + " { ";
			break;
		}
		case bsontype::INT32BIT: {
			elemStr += (std::to_string(bsontype::INT32BIT));
			elemStr += " " + this->fieldName + " : ";
			elemStr += std::to_string(this->getInt());
			break;
		}
		case bsontype::TIMESTAMP: {
			elemStr += (std::to_string(bsontype::TIMESTAMP));
			elemStr += " " + this->fieldName + " : ";
			elemStr += std::to_string(this->getInt64());
			break;
		}
		case bsontype::INT64BIT: {
			elemStr += (std::to_string(bsontype::INT64BIT));
			elemStr += " " + this->fieldName + " : ";
			elemStr += std::to_string(this->getInt64());
			break;
		}
		case bsontype::MINKEY: {
			elemStr += (std::to_string(bsontype::MINKEY));
			elemStr += " " + this->fieldName + " : ";
			elemStr += "1";
			break;
		}
		case bsontype::MAXKEY: {
			elemStr += (std::to_string(bsontype::MAXKEY));
			elemStr += " " + this->fieldName + " : ";
			elemStr += "1";
			break;
		}
		case bsontype::EOO: {
			arrayOpened ? elemStr += "]" : elemStr += " }";
			arrayOpened = false;
			break;
		}
		}
		return elemStr;
	}

	std::vector<unsigned char> BsonElement::getBson() {
		std::vector<unsigned char> bsonRaw;
		bsonRaw.push_back(this->getType());
		if(this->getType() != bsontype::JSCODESCOPE) // if scope fieldName sould not be included
			bsonRaw.insert(bsonRaw.end(), this->fieldName.begin(), this->fieldName.end());
		bsonRaw.insert(bsonRaw.end(), this->value.begin(), this->value.end());
		return bsonRaw;
	}

	bool BsonElement::isEmpty() {
		return this->value.empty();
	}
}