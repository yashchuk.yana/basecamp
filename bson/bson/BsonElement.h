#pragma once
#include "bsonTypes.h"

namespace bsonlib {
	/** Element class is used for storing key-value pairs.
		All values are stored according to BSON specification http://bsonspec.org/spec.html	
	*/
	class BsonElement {
		int size = 1; // Holds current size of entire element in bytes
		bsontype type;
		std::string fieldName;
		std::vector<unsigned char> value; 

		/** Template initialization function for creating simple elements */
		template <typename T>
		void init(std::string fname, T value, bsontype type);
	public:
		/** Initializes empty element of type EOO */
		BsonElement();

		/** Creates empty element of given type */
		BsonElement(std::string fname, bsontype type);

		/** Constructs element with given bool value */
		BsonElement(std::string fname, bool value);

		/** Constructs element with given int value,
			if isArray flag is provided - constructs array and copies its size to value,
			if isDoc flag is provided - constructs object and copies its size to value,
			if isScope flag is provided - constructs scope object and copies its size to value
		*/
		BsonElement(std::string fname, int32_t value, bool isArray = false, bool isDoc = false, bool isScope = false);

		/** Constructs element with given 64-bit int value,
			if isUTCDateTime flag is provided - constructs element with given UTCDateTime value
		*/
		BsonElement(std::string fname, int64_t value, bool UTCDateTime = false);

		/** Constructs element with given 64-bit uint timestamp value */
		BsonElement(std::string fname, uint64_t value);

		/** Constructs element with given 64-bit double value */
		BsonElement(std::string fname, double value);

		/** Constructs element with given string value,
			if isOID flag is provided - constructs OID element from given string - string must be 24-chars long and hex
			if isCode flag is provided - constructs element with given code value,
			copies value size as first 4 bytes of value
		*/
		BsonElement(std::string fname, std::string value, bool isOID = false, bool isCode = false);

		/** Constructs element with given regex template with regexOptions */
		BsonElement(std::string fname, std::string regex, std::string regexOptions);

		/** Constructs OID element from raw 12-bytes long array */
		BsonElement(std::string fname, unsigned char * oid);

		/** Constructs binary element in a following format
			<field name> <size subtype (byte* data)>
		*/
		BsonElement(std::string fname, int size, bsonsubtype subtype, unsigned char * bindata);

		/** Getters and setters */
		int getSize();
		std::string getFieldName();
		void setFieldName(std::string fieldName);
		bsontype getType();
		void setType(bsontype type);
		std::vector<unsigned char> getValue();
		void setValue(unsigned char * data, int size);

		/** Special setter for bool type */
		void setBoolValue(bool value);

		/** Returns human-readable representation of an element, not a JSON */
		std::string toString();

		/** Returns BSON-formatted raw vector */
		std::vector<unsigned char> getBson();
		
		/** Extracts desired values from elements,
			return 0 if called on element of wrong type
		*/
		std::string getUTF();
		int getInt();
		int64_t getInt64();
		double getDouble();

		/** True if elements value is empty */
		bool isEmpty();

	};

	template <typename T>
	void BsonElement::init(std::string fname, T value, bsontype type) {
		this->type = type;
		this->fieldName = fname;
		if (type != bsontype::JSCODESCOPE) // if scope fieldName should be empty
			this->fieldName += '\0';
		const unsigned char* ptr = reinterpret_cast<const unsigned char*>(&value);
		for (size_t i = 0; i < sizeof(T); ++i)
			this->value.push_back(ptr[i]);
		this->size += this->value.size() + this->fieldName.size();
	}
}