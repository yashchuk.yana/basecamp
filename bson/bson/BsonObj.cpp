#include "BsonObj.h"
#include <sstream>

namespace bsonlib {

	BsonObj::BsonObj() {
		this->appendEOO();
	}

	BsonObj::BsonObj(std::istream &in) {
		int totalSize = 0;
		int next = 0;
		for (int i = 0; i < sizeof(int); ++i) // parse object size -  4 first bytes
		{
			next = in.get();
			if (next >= 0)
				totalSize += (next << (8 * i));
		}
		this->bson.reserve(totalSize);
		bool parse = true;
		bool objectParse = false;
		while (parse && in.good()) {
			bsontype type = (bsontype)in.get();
			std::string fieldName;
			if (type != bsontype::EOO && type!= bsontype::JSCODESCOPE) { // if EOO or SCOPE fieldname is empty
				while (in.peek() != '\0') {
					fieldName += in.get();
				}
				in.ignore();
			}
			BsonElement elemNew;
			elemNew.setType(type);
			elemNew.setFieldName(fieldName);
			switch (type)
			{
			case bsontype::EOO: {
				if(!objectParse)
					parse = false; // stop parsing
				objectParse = false; // stop parsing embedded object
				break;
			}
			case bsontype::DOUBLE64BIT: {
				const size_t size = sizeof(double);
				unsigned char rawVal[size];
				in.readsome((char*)rawVal, size);
				elemNew.setValue(rawVal, size);
				break;
			}
			case bsontype::JSCODE:
			case bsontype::UTF8STRING: {
				std::string val = this->extractStrFromStream(in);
				elemNew.setValue((unsigned char*)val.c_str(), val.size());
				break;
			}
			case bsontype::DOCUMENT: 
			case bsontype::ARRAY: {
				objectParse = true;
				const size_t size = sizeof(int);
				unsigned char rawVal[size];
				in.readsome((char*)rawVal, size);
				elemNew.setValue(rawVal, size);
				break;
			}
			case bsontype::BINARYDATA: {
				int sizeStr = 0;
				int sizeBit = 0;
				std::string value;
				for (int i = 0; i < sizeof(int); ++i) {
					sizeBit = in.get();
					sizeStr += (sizeBit) << (8 * i);
					value += sizeBit;
				}
				value += in.get(); // subtype
				for (int i = 0; i < sizeStr; i++) {
					value += in.get();
				}
				elemNew.setValue((unsigned char*)value.c_str(), value.size());
				break;
			}
			case bsontype::OBJECTID: {
				unsigned char oid[OID_SIZE];
				for (int i = 0; i < OID_SIZE; i++) {
					oid[i] = in.get();
				}
				elemNew.setValue(oid, OID_SIZE);
				break;
			}
			case bsontype::BOOLFALSETRUE: {
				elemNew.setBoolValue(in.get());
				break;
			}
			case bsontype::NULLVALUE:
			case bsontype::MINKEY:
			case bsontype::MAXKEY:
				break; // nothing to do, value is empty
			case bsontype::REGEX: {
				std::string regex;
				while (in.peek() != '\0') {
					regex += in.get();
				}
				regex += in.get();
				std::string regexOpt;
				while (in.peek() != '\0') {
					regexOpt += in.get();
				}
				regexOpt += in.get();
				regex.insert(regex.end(), regexOpt.begin(), regexOpt.end());
				elemNew.setValue((unsigned char*)regex.c_str(), regex.size());
				break;
			}
			case bsontype::JSCODESCOPE: {
				const size_t size = sizeof(int);
				objectParse = true;
				unsigned char rawVal[size];
				in.readsome((char*)rawVal, size);
				elemNew.setValue(rawVal, size);
				break;
			}
			case bsontype::INT32BIT: {
				int value = 0;
				const size_t size = sizeof(int);
				unsigned char rawVal[size];
				in.readsome((char*)rawVal, size);
				elemNew.setValue(rawVal, size);
				break;
			}
			case bsontype::TIMESTAMP: {
				uint64_t value = 0;
				const size_t size = sizeof(uint64_t);
				unsigned char rawVal[size];
				in.readsome((char*)rawVal, size);
				elemNew.setValue(rawVal, size);
				break;
			}
			case bsontype::UTCDATETIME:
			case bsontype::INT64BIT: {
				int64_t value = 0;
				const size_t size = sizeof(int64_t);
				unsigned char rawVal[size];
				in.readsome((char*)rawVal, size);
				elemNew.setValue(rawVal, size);
				break;
			}
			default:
				break;
			}
			this->appendBsonElement(elemNew);
		}
	}

	std::string BsonObj::toString() {
		std::string bsonStr = std::to_string(this->totalSize) + " {";
		for (auto it = this->bson.begin(); it != this->bson.end(); ++it) {
			bsonStr.append(it->toString());
			bsonStr.append(",\n ");
		}
		return bsonStr;
	}

	std::vector<unsigned char> BsonObj::getBson() {
		std::vector<unsigned char> bsonRaw;
		// fill first 4 bytes with object size
		bsonRaw.push_back(this->totalSize);
		bsonRaw.push_back(this->totalSize >> 8);
		bsonRaw.push_back(this->totalSize >> 16);
		bsonRaw.push_back(this->totalSize >> 24);
		// append all elements in raw bson format
		for (auto it = this->bson.begin(); it != this->bson.end(); ++it) {
			std::vector<unsigned char> temp = it->getBson();
			bsonRaw.insert(bsonRaw.end(), temp.begin(), temp.end());
		}
		return bsonRaw;
	}

	void BsonObj::exportToStream(std::ostream &out) {
		std::vector<unsigned char> raw = this->getBson();
		out.write(reinterpret_cast<char*>(&raw[0]), raw.size() * sizeof(raw[0]));
	}

	std::string BsonObj::extractStrFromStream(std::istream &in) {
		int sizeStr = 0;
		std::string value;
		for (int i = 0; i < sizeof(int); ++i) { // extract size and place it in first 4 bytes
			int sizeBit = in.get();
			sizeStr += (sizeBit) << (8 * i);
			value += sizeBit;
		}
		for (int i = 0; i < sizeStr; i++) {
			value += in.get();
		}
		return value;
	}


	int BsonObj::getSize() {
		return this->totalSize;
	}

	bool BsonObj::isEmpty() {
		return this->bson[0].getType() == bsontype::EOO;
	}

	void BsonObj::increaseSize(int valToIncrease){
		this->totalSize += valToIncrease;
	}

	void BsonObj::appendBsonElement(BsonElement &elem) {
		this->bson.push_back(elem);
		this->increaseSize(elem.getSize());
	}


	void BsonObj::appendEOO() {
		this->bson.push_back(BsonElement());
		this->increaseSize(1);
	}


	void BsonObj::appendEndArray() { 
		this->bson.push_back(BsonElement());
	}

	void BsonObj::appendNull(std::string key) {
		this->appendValue(key, bsontype::NULLVALUE);
	}

	void BsonObj::appendUTF8(std::string key, std::string value) {
		this->appendValue(key, value);
	}

	void BsonObj::appendBool(std::string key, bool value) {
		this->appendValue(key, value);
	}

	void BsonObj::appendInt32(std::string key, int32_t value) {
		this->appendValue(key, value);
	}

	void BsonObj::appendInt64(std::string key, int64_t value) {
		this->appendValue(key, value);
	}

	void BsonObj::appendDouble(std::string key, double value) {
		this->appendValue(key, value);
	}

	void BsonObj::appendTimestamp(std::string key, uint64_t timestamp) {
		this->appendValue(key, timestamp);
	}

	void BsonObj::appendOIDFromStr(std::string key, std::string oid) {
		if (oid.size() == 24) {
			BsonElement elem(key, oid, true);
			this->bson.insert(this->bson.end() - 1, elem);
			this->increaseSize(elem.getSize());
		}
	}

	void BsonObj::appendOIDFromArray(std::string key, unsigned char * oid) {
		this->appendValue(key, oid);
	}
	
	void BsonObj::appendUTCDateTime(std::string key, int64_t seconds) {
		BsonElement elem(key, seconds, true);
		this->bson.insert(this->bson.end() - 1, elem);
		this->increaseSize(elem.getSize());
	}

	void BsonObj::appendArray(std::string key, std::vector<BsonElement> values) {
		this->bson.pop_back(); // delete eoo
		this->totalSize--;
		int arraySize = 0;
		for (auto it = values.begin(); it != values.end(); ++it) {
			arraySize += it->getSize();
		}
		this->increaseSize(arraySize);
		BsonElement startArray(key, arraySize + 4 + 1, true, false); // +4 for size +1 for end of array
		this->bson.push_back(startArray);
		this->bson.insert(this->bson.end(), values.begin(), values.end());
		this->increaseSize(startArray.getSize());
		this->appendEndArray();
		this->appendEOO();
	}

	void BsonObj::appendDocument(std::string key, BsonObj document) {
		this->bson.pop_back(); // delete eoo
		this->totalSize--;
		this->increaseSize(document.getSize() - 4); // size of doc will be the value of start element
		BsonElement startDocument(key, document.getSize(), false, true);
		this->bson.push_back(startDocument);
		this->bson.insert(this->bson.end(), document.bson.begin(), document.bson.end());
		this->increaseSize(startDocument.getSize());
		this->appendEOO();
	}

	void BsonObj::appendBinary(std::string key, int size, bsonsubtype subtype, unsigned char * bindata) {
		BsonElement elem(key, size, subtype, bindata);
		this->bson.insert(this->bson.end() - 1, elem);
		this->increaseSize(elem.getSize());
	}

	void BsonObj::appendRegex(std::string key, std::string regex, std::string regexOptions) {
		BsonElement elem(key, regex, regexOptions);
		this->bson.insert(this->bson.end() - 1 , elem);
		this->increaseSize(elem.getSize());
	}

	void BsonObj::appendJSCode(std::string key, std::string code) {
		BsonElement elem(key, code, false, true);
		this->bson.insert(this->bson.end() - 1, elem);
		this->increaseSize(elem.getSize());
	}

	void BsonObj::appendJSCodeScope(std::string key, std::string code, BsonObj *document) {
		this->appendJSCode(key, code);
		if (document != nullptr) {
			this->bson.pop_back(); // delete eoo
			BsonElement startDocument("", document->getSize() + this->getSize(), false, false, true);
			this->bson.push_back(startDocument);
			this->bson.insert(this->bson.end(), document->bson.begin(), document->bson.end());
			this->increaseSize(document->getSize());
			this->appendEOO();
		}
	}

	void BsonObj::appendMaxKey(std::string key) {
		this->appendValue(key, bsontype::MAXKEY);
	}

	void BsonObj::appendMinKey(std::string key) {
		this->appendValue(key, bsontype::MINKEY);
	}

	bool BsonObj::hasField(std::string key) {
		bool hasField = false;
		for (auto elem : this->bson) {
			if (elem.getFieldName() == key) {
				hasField = true;
			}
		}
		return hasField;
	}

	BsonElement BsonObj::getField(std::string key) {
		BsonElement found;
		for (auto elem : this->bson) {
			if (elem.getFieldName() == key) {
				found = elem;
			}
		}
		return found;
	}

	BsonObj BsonObj::getObjectField(std::string key) {
		BsonObj found;
		bool arrayStart = false;
	
		for (auto elem : this->bson) {
			bool isDoc = elem.getType() == bsontype::DOCUMENT;
			bool isArray = elem.getType() == bsontype::ARRAY;
			if (elem.getFieldName() == key && (isDoc || isArray)) {
				found.bson.pop_back(); // delete eoo
				found.totalSize--;
				BsonElement startDocument(key, elem.getSize(), isArray, isDoc);
				found.bson.push_back(startDocument);
				arrayStart = true;
			}
			else if (arrayStart == true && elem.getType() != bsontype::EOO) {
				found.bson.push_back(elem);
				found.increaseSize(elem.getSize());
			}
			else if (arrayStart == true && elem.getType() == bsontype::EOO) {
				found.appendEOO();
				arrayStart = false;
				break;
			}
		}
		return found;
	}

	BsonElement BsonObj::getFieldDotted(std::string key) {
		BsonElement elem = getField(key);
		if (elem.isEmpty()) {
			size_t beforeDot = key.find('.');
			if (beforeDot != std::string::npos) {
				std::string left = key.substr(0, beforeDot);
				std::string right = key.substr(beforeDot + 1);
				BsonObj sub = getObjectField(left);
				return sub.isEmpty() ? BsonElement() : sub.getFieldDotted(right);
			}
		}
		return elem;
	}

	BsonElement BsonObj::operator[](std::string key) {
		return this->getField(key);
	}

}