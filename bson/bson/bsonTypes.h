﻿#pragma once

#include <vector>
#include <string>

namespace bsonlib {
/** maximum length of objectId */
#define OID_SIZE 12

	typedef enum bsontype {
		/** end of object */
		EOO = 0x00,
		/** double floating point number */
		DOUBLE64BIT = 0x01,
		/** string encoded in utf-8 format */
		UTF8STRING = 0x02,
		/** embedded object */
		DOCUMENT = 0x03,
		/** embedded array */
		ARRAY = 0x04,
		/** raw binary data, see subtypes */
		BINARYDATA = 0x05,
		/** objectId, holds 12 bytes */
		OBJECTID = 0x07,
		/** bool value */
		BOOLFALSETRUE = 0x08,
		/** 64-bit UTC datetime */
		UTCDATETIME = 0x09,
		/** null value */
		NULLVALUE = 0x0A,
		/** regex pattern with options */
		REGEX = 0x0B,
		/** JavaScript code */
		JSCODE = 0x0D,
		/** JavaScript code with scope in which the string should be evaluated */
		JSCODESCOPE = 0x0F,
		/** 32-bit integer */
		INT32BIT = 0x10,
		/** 64-bit unsigned timestamp */
		TIMESTAMP = 0x11,
		/** 64-bit integer value */
		INT64BIT = 0x12,
		/** Min key */
		MINKEY = 0xFF,
		/** Max key */
		MAXKEY = 0x7F
	} bsontype;

	/** binary value subtypes */
	typedef enum bsonsubtype {
		GENERIC = 0x00,
		FUNCTION = 0x01,
		BINARYOLD = 0x02,
		UUIDOLD = 0x03,
		UUID = 0x04,
		MD5 = 0x05,
		USERDEFINED = 0x80
	} bsonsubtype;
}
