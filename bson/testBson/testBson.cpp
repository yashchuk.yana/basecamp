#include <iostream>
#include <fstream>
#include "BsonObj.h"

using namespace std;
using namespace bsonlib;

int main()
{
	// Creating object for serializing 
	BsonObj testToFile;
	testToFile.appendDouble("double", 3.1);
	testToFile.appendUTF8("utf8", "hello");

	// Test nested object
	BsonObj nested;
	nested.appendInt32("dummy", 2);
	testToFile.appendDocument("object", nested);

	// Test binary
	unsigned char cmd1[] = { 0xfe, 0x58 };
	testToFile.appendBinary("binary", sizeof(cmd1), bsonsubtype::USERDEFINED, cmd1);

	testToFile.appendOIDFromStr("oid", "507f1f77bcf86cd799439011");
	testToFile.appendBool("bool", false);
	testToFile.appendUTCDateTime("utc", 1294548238);
	testToFile.appendNull("null");
	testToFile.appendRegex("regex", "W+", "+.");
	testToFile.appendJSCode("code", "alert(\"Hi\");");

	// Test JSCode with scope
	BsonObj * scope = new BsonObj;
	scope->appendNull("null");
	testToFile.appendJSCodeScope("codescope", "code", scope);

	testToFile.appendTimestamp("time", 1412180887);
	testToFile.appendMaxKey("max");
	testToFile.appendMinKey("min");

	// Export object to file 
	std::ofstream outFile;
	outFile.open("Test.bson", std::ios::out | std::ios::binary);
	if(outFile.is_open())
		testToFile.exportToStream(outFile);
	outFile.close();

	// Read "oid" field from object
	cout << "OID object search returned: " << testToFile["oid"].toString() << endl;

	// Dot notation search in nested object
	cout << "Dot notation search returned: " << testToFile.getFieldDotted("object.dummy").toString() << endl;

	// Check if field is present in object
	cout << "Check for binary field returned: " << testToFile.hasField("binary") << endl;

	// Search not existing field - isEmpty() is true
	cout << "isEmpty() of not existing field: " << testToFile.getField("not a field").isEmpty() << endl;
	cout << "isEmpty() of not existing object: " << testToFile.getObjectField("none").isEmpty() << endl;

	// Import BSON from file to object
	std::ifstream inFile;
	inFile.open("Test.bson", std::ios::in | std::ios::binary);
	if (inFile.is_open()) {
		BsonObj testFromFile(inFile);
		cout << "\n\n Object from file: \n" << testFromFile.toString() << endl;
	}
	inFile.close();
	
	return 0;
}